import React, { Component } from 'react'

//props จะโยนข้าม component ได้
//state ใช้ภาพใน compomemt หรือ class นั้นๆ

export default class Demo1 extends Component {
    
    constructor(props){
        super(props)
    }

    render() {
        return (
            <div>
                <span>{this.props.name} </span>
                <input type={this.props.type} name="" id="input" placeholder={this.props.hint} />
            </div>
        )
    }
}
