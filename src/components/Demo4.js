
import React, { Component } from 'react'
import PropTypes from 'prop-types'

export default class Demo4 extends Component {
  render() {
    return (
      <div>
        {this.props.text} {this.props.count}
      </div>
    )
  }
}


Demo4.propTypes = {
    text: PropTypes.string,
    count: PropTypes.number
}

Demo4.defaultProps = {
    text: 'CHEDDO',
    count:'123213123'
}