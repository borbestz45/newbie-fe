import React, { Component } from 'react'
import ReactDOM from 'react-dom'
export default class Demo6 extends Component {

    

    constructor(props) {
        super(props)

        this.state = {
            count1: 0,
            count2: 0,
        }
    }
    render() {
        // this.state.count1  this.state.count2
        // this.forceUpdate ต้องการที่ render ใหม่โดยไม่สนใจ state หรือ props
        const { count1, count2 } = this.state

        return (
            <div>
                <button onClick={() => this.setState({ count1: this.state.count1 + 1 })} style={{ backgroundColor: 'pink' }}>SetState One : {this.state.count1}</button>
                <button onClick={() =>
                    this.setState(prevState => {
                        return { count2: prevState.count1 + prevState.count2 }
                    })}
                    style={{ backgroundColor: 'green' }}>SetState Two : {count2}
                </button>
                <button onClick={() => this.forceUpdate()} style={{ backgroundColor: 'yellow' }}>forceUpdate : {Math.random()}</button>
                <button onClick={() => {
                    var myDiv = document.getElementById('comment')
                    ReactDOM.findDOMNode(myDiv).style.backgroundColor = 'pink'
                }}>
                    fine Element
                </button>
                <div id="comment">CHEDDO TECH</div>
            </div>
        )
    }
}
