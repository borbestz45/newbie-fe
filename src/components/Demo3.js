import React, { Component } from 'react'

export default class Demo3 extends Component {

    constructor(props) {
        super(props)
        this.state = { count: 0, message: 'RESET', text: '', items: 0, }
    }


    addBox = () => {
        let box = []
        for (var i = 0; i < this.state.items; i++) {
            box.push(
                <div style={{ backgroundColor: 'red', width: 40, height: 50, margin:5 }}>
                </div>
            )
        }
        return box
    }

    //this.state เรียกใช้ค่าที่อยู่ใน state
    //this.set.state เซตค่าเข้าไปใน state
    render() {
        return (

            <div>
                <button
                    style={{ width: 100 }}
                    onClick={() => {
                 
                        this.setState({ count: this.state.count + 1 },()=>{
                            if (this.state.count % 5 === 0 && this.state.count !== 0) {
                                this.setState({ items: this.state.items + 1 })
                            }
                        })
                   
                    }}>
                    {this.state.count}
                </button>
                <button
                    style={{ width: 100 }}
                    onClick={() => {
                        this.setState({ count: 0, message: 'Clear', text: '' })
                    }}>
                    {this.state.message}
                </button>
                {
                    // this.state.count % 5 === 0 && (
                    this.addBox()
                    // )
                }
                {/* <input value={this.state.text}  onChange={(e)=> { this.setState({text: e.target.value})}}/>
                <div>{this.state.text}</div> */}
            </div>
        )
    }
}
