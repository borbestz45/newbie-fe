import React, { Component } from 'react'
// import Demo1 from '../'
export default class Demo7 extends Component {

    constructor(props) {
        super(props)

        this.state = {
            data: 0,
            data1: 0
        }
    }
    render() {
        return (
            <div>
                <button onClick={() => this.setState({ data: this.state.data + 1 })}>INCREMENT</button>
                <Content myNumber={this.state.data} />
                {this.state.data1}
            </div>
        )
    }
}

class Content extends Component {

    constructor(props) {
        super(props)
        console.log('Component Constructing')
    }

    //เข้าก่อน Render ครั้งเดียว
    componentWillMount() {
        console.log('Component WillMount')
    }
    //เข้าตอน Render ครั้งเดียว
    componentDidMount() {
    
            
    }

    shouldComponentUpdate() {
        return true
    }
    // เข้าตอนที่ props มีการเปลี่ยนแปลง
    componentWillReceiveProps() {
        console.log('Component Will Receive Props')
    }
    // เข้าตอนที่ props หรือ state มีการเปลี่ยนแปลง
    componentDidUpdate() {
        console.log("Component DidUpdate")
    }
    // componentWillUnmount() {
    //     console.log("Component WillUnmount")
    // }

    render() {
        return (
            <div>
                <h3>{this.props.myNumber}</h3>
            </div>
        )
    }
}

